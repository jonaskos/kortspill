package edu.ntnu.jonaskos.cardgame.gui;
import edu.ntnu.jonaskos.cardgame.Hand;
import edu.ntnu.jonaskos.cardgame.PlayingCard;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

import java.util.ArrayList;


public class Stage extends Application{


    public static void main(String[] args){
        launch(args);
    }
    @Override
    public void start(javafx.stage.Stage primaryStage) throws Exception {

        Hand hand = new Hand();
        ObservableList<String> cards = FXCollections.observableArrayList();

        VBox vbox = new VBox();

        HBox topControls = new HBox();
        VBox.setMargin(topControls, new Insets(10.0d));
        topControls.setAlignment(Pos.BOTTOM_LEFT);

        Button dealHand = new Button("deal hand");
        Button checkHand = new Button("Check hand");

        Label sumofFaces = new Label("Sum of the faces: ");
        Label cardsofHearts = new Label("Cards of hearts: ");
        Label flush = new Label("Flush: ");
        Label queenofSpades = new Label("Queen of spades: ");

        TextField sumofFacesResult = new TextField();
        TextField cardsofHeartsResult = new TextField();
        TextField flushResult = new TextField();
        TextField queenofSpadesResult = new TextField();
        sumofFacesResult.setEditable(false);
        cardsofHeartsResult.setEditable(false);
        flushResult.setEditable(false);
        queenofSpadesResult.setEditable(false);


        topControls.getChildren().addAll(dealHand, checkHand);


        //GridPane


        ListView<String> cardsList = new ListView<>(cards);

        cardsList.setMaxHeight(120);
        VBox cardDisplay = new VBox();

        GridPane checks = new GridPane();

        checks.add(sumofFaces,0,0);
        checks.add(sumofFacesResult,1,0);
        checks.add(cardsofHearts,0,1);
        checks.add(cardsofHeartsResult,1,1);
        checks.add(flush,2,0);
        checks.add(flushResult,3,0);
        checks.add(queenofSpades,2,1);
        checks.add(queenofSpadesResult,3,1);

        cardDisplay.getChildren().add(cardsList);
        //cardDisplay.getChildren().add(cardsView);



        vbox.getChildren().addAll(topControls,cardDisplay);
        vbox.getChildren().add(checks);

        dealHand.setOnAction(e ->{
            cards.clear();
            hand.drawCards();
            for(int i=0;i < hand.getHand().size(); i++){
                cards.add(hand.getHand().get(i).getAsString());
            }
        });

        checkHand.setOnAction(e -> {
            try{
                if(hand.isFlush()){
                    flushResult.setText("Yes");
                } else{
                    flushResult.setText("No");
                }
                if(hand.isQueenSpadePresent()){
                    queenofSpadesResult.setText("Yes");
                } else{
                    queenofSpadesResult.setText("No");
                }
                sumofFacesResult.setText(String.valueOf(hand.getSum()));
                cardsofHeartsResult.setText(hand.getHearts().toString());



            }catch (NullPointerException error){
                sumofFacesResult.setText("N/A");
                cardsofHeartsResult.setText("N/A");
                flushResult.setText("N/A");
                queenofSpadesResult.setText("N/A");
            }

        });

        Scene scene = new Scene(vbox, 500, 500);
        primaryStage.setTitle("Cardgame");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
