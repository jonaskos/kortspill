package edu.ntnu.jonaskos.cardgame;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class DeckOfCards {

    private final char[] suit ={'S','H','D','C'};
    private ArrayList<PlayingCard> cardsInDeck;



    public DeckOfCards(){
        this.cardsInDeck = new ArrayList<>();
        for(int i = 1; i <= 13; i++){
            for(int j = 0; j < 4; j++){
                cardsInDeck.add(new PlayingCard(suit[j],i));
            }
        }


    }

    public ArrayList<PlayingCard> dealHand(int n) throws IllegalArgumentException{
        if(n > 52 || n <= 0){
            throw new IllegalArgumentException("Provide a value between 1 and 52");
        }
        ArrayList<PlayingCard> randomCards = cardsInDeck;
        Collections.shuffle(randomCards);
        ArrayList<PlayingCard> cardsReturned = new ArrayList<>();
        for(int i = 0; i<n; i++){
            cardsReturned.add(randomCards.get(i));
        }


        return cardsReturned;
    }


    public ArrayList<PlayingCard> getCards(){
        return cardsInDeck;
    }


    @Override
    public String toString() {
        return "Deck: " + cardsInDeck + "";
    }
}
