package edu.ntnu.jonaskos.cardgame;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Hand {
    DeckOfCards deck;
    ArrayList<PlayingCard> hand;

    public Hand(){
        this.deck = new DeckOfCards();
    }

    public Hand(ArrayList<PlayingCard> hand){
        this.deck = new DeckOfCards();
        this.hand = hand;
    }

    public void drawCards(){
        this.hand = deck.dealHand(5);
    }

    public ArrayList<PlayingCard> getHand(){
        return hand;
    }

    public int getSum() throws NullPointerException{
        if (hand.stream().map(PlayingCard::getFace).reduce(Integer::sum).isPresent())
        return hand.stream().map(PlayingCard::getFace).reduce(Integer::sum).get();
        else return -1;
    }

    public ArrayList<PlayingCard> getHearts() throws NullPointerException{
        return hand.stream().filter(p -> p.getSuit() == 'H').collect(Collectors.toCollection(ArrayList::new));
    }

    public boolean isQueenSpadePresent(){
        return hand.stream().anyMatch(p -> p.getSuit() == 'S' && p.getFace() == 12);
    }

    public boolean isFlush(){
        return hand.stream().allMatch(p -> p.getSuit() == 'S') ||
                hand.stream().allMatch(p -> p.getSuit() == 'H') ||
                hand.stream().allMatch(p -> p.getSuit() == 'D') ||
                hand.stream().allMatch(p -> p.getSuit() == 'C');
    }


}
