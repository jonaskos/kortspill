package edu.ntnu.jonaskos.cardgame;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class HandTest {


    @Test
    void isQueenSpadePresent() {
        ArrayList<PlayingCard> handWithQueenSpades = new ArrayList<>();
        for(int i = 1; i < 5; i++){
            handWithQueenSpades.add(new PlayingCard('S', i));
        }
        handWithQueenSpades.add(new PlayingCard('S', 12));
        Hand handQueenSpades = new Hand(handWithQueenSpades);

        assertTrue(handQueenSpades.isQueenSpadePresent());
    }

    @Test
    void isFlush() {
        ArrayList<PlayingCard> handWithSameSuit = new ArrayList<>();
        for(int i = 1; i < 6; i++){
            handWithSameSuit.add(new PlayingCard('S', i));
        }
        Hand handFlush = new Hand(handWithSameSuit);

        assertTrue(handFlush.isFlush());
    }

    @Test
    void isNotFlush(){
        ArrayList<PlayingCard> handWithNotSameSuit = new ArrayList<>();
        for(int i = 1; i < 5;i++){
            handWithNotSameSuit.add(new PlayingCard('S', i));
        }
        handWithNotSameSuit.add(new PlayingCard('C', 12));
        Hand handNoFlush = new Hand(handWithNotSameSuit);
        assertFalse(handNoFlush.isFlush());
    }
}