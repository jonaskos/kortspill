package edu.ntnu.jonaskos.cardgame;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardsTest {

    DeckOfCards deck = new DeckOfCards();


    @Test
    void CorrectAmount() {
        assertEquals(52, deck.getCards().size());
    }

    @Test
    void DealHandTCorrectAmount() {
        ArrayList<PlayingCard> cards = deck.dealHand(5);

        assertEquals(5, cards.size());
    }

    @Test
    void VerifyNoSameCards(){
       Long differentCards = deck.getCards().stream().distinct().count();

       assertEquals(52, differentCards);
    }

}